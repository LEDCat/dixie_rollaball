﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System.Security.Cryptography;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;

    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    public int count;
    private Rigidbody rb;
    private float movementX;
    private float movementY;

    Renderer rend; //renderer component
    public Material[] randomMaterials; //array filled with custom materials
    int currentMaterialIndex = 0; //prevents same material getting chosen twice

    public float jumpPower; //how much force the jump has
    bool onGround = false; //if the ball is resting on the ground

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);

        rend = GetComponent<Renderer>();
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 13)
        {
            winTextObject.SetActive(true);
        }
    }

    void RandomColor()
    {
        int randomIndex = Random.Range(0, randomMaterials.Length);
        while (randomIndex == currentMaterialIndex) //if randomly picked material is the current material
        {
            randomIndex = Random.Range(0, randomMaterials.Length); //keeps picking materials until a different one is chosen
        }
        currentMaterialIndex = randomIndex; //an index that hold the current color chosen
        rend.material = randomMaterials[currentMaterialIndex]; //sets material to the one in the array at the index
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        
        rb.AddForce(movement * speed);
    }

    private void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, .51f);

        if (transform.position.y < -10f) //if player has fallen far enough
            //reloads game scene
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void OnJump()
    {
        Jump(); //jumping mechanic
    }
    void Jump()
    {
        if (onGround) //allows player to jump if touching the ground
            rb.AddForce(Vector3.up * jumpPower);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
            RandomColor(); //changes set material
        }
    }

}
